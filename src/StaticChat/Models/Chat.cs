﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StaticChat.Models
{
    public static class Chat
    {
        static object o = new object();
        public static List<Message> Messages; 

        static Chat()
        {
            Messages = new List<Message>() {
                new Message { Text = "Первое тестовое сообщение.", Sign = "Автор1"},
                new Message { Text = "Второе тестовое сообщение.", Sign = "Автор2"},
                new Message { Text = "Третье тестовое сообщение.", Sign = "Автор3"},
            }; 
        }

        public static void Add(Message mes)
        {
            lock (o)
            {
                Messages.Add(mes);
            }
        }
    }
}
