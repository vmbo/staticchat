﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StaticChat.Models
{
    public class Message
    {
        public string Text { get; set; }
        public string Sign { get; set; }
    }
}
