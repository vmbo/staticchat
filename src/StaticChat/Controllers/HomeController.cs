﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using StaticChat.Models;

namespace StaticChat.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            var model = new IndexViewModel { Messages = Chat.Messages };
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(Message mes)
        {
            Chat.Add(mes);

            var model = new IndexViewModel { Messages = Chat.Messages };
            return View(model);
        }
    }
}
